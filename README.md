# RPi Clock with Auto Brightness
This is an rpi1-based integration between TM1637 7-Segment Display and BH1750 Light Sensor written in Node.js. Using modified version of "[`raspberrypi-tm1637`](https://www.npmjs.com/package/raspberrypi-tm1637)".

## Configuration
`config.json`
```
{
    // BH1750 Configuration
    "BH1750": {
        "ENABLED": true, // BH1750 function can be disabled here
        "I2C_PORT": 0, // I2C Port Index (typically 0 for RPi 1)
        "I2C_ADDRESS": 35 // I2C Address (default 0x23 (decimal 35) for BH1750)
    },
    // TM1637 Configuration 
    "TM1637": {
        "CLKPIN": 2, // Clock Pin (WiringPi Pin Number)
        "DIOPIN": 0, // Data IO Pin (WiringPi Pin Number)
        "DEFAULT_BRIGHTNESS": 7 // Fallback brightness if BH1750 is disabled or failed
    },
    "DEBUG": false, // Enable Verbose Logging
    "brightness_levels": [
        {
            "minLux": 50, // Minimum Light in Lux for the level
            "brightness": 7 // Display Brightness Level (1 - 7)
        },
        {
            "minLux": 5,
            "brightness": 2
        },
        {
            "minLux": 0,
            "brightness": 1
        }
    ]
}
```

## Installation
To install the program, you can use [`pm2`](https://www.npmjs.com/package/pm2) to autostart it.

```
# Install WiringPi (Required)
sudo apt-get install python-dev python-pip
sudo pip install wiringpi

# Install Node.js
# See https://www.nodejs.org/ for more details

# Install pm2
npm -g install pm2

# Download from Repository
git clone https://gitlab.com/44kia244/rpi-clock-with-auto-brightness

# Install Dependencies
cd rpi-clock-with-auto-brightness
npm install
cd ..

# Add to pm2 autostart
pm2 start ./rpi-clock-with-auto-brightness
```

## Dependencies
- [WiringPi](http://wiringpi.com/)
- [Node.js](https://www.nodejs.org)
- [Moment.js](https://www.npmjs.com/package/moment)
- [i2c-bus](https://www.npmjs.com/package/i2c-bus)
- [node-wiring-pi](https://www.npmjs.com/package/node-wiring-pi)
