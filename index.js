const moment = require('moment');
const TM1637 = require('./tm1637');
const BH1750 = require('./bh1750');
const config = require('./config');

const DEBUG = config.DEBUG;
const brightnessLevel = config.brightness_levels;

const tm = new TM1637(config.TM1637.CLKPIN, config.TM1637.DIOPIN);
const bh = new BH1750(config.BH1750.I2C_PORT, config.BH1750.I2C_ADDRESS)
const BH_ENABLED = !!config.BH1750.ENABLED

async function getBrightness (defaultBrightness = 7) {
    try {
        if (BH_ENABLED) {
            let currentLux = await bh.getLux()
            let level = brightnessLevel.find(function(l) { return l.minLux <= currentLux })
            if (!level) level = brightnessLevel[0]
            if (DEBUG) console.log('[DEBUG] ' + currentLux.toFixed(2) + ' lx, brightness ' + level.brightness)
    
            return level.brightness
        } else {
            return defaultBrightness
        }
    } catch (e) {
        console.error('[ERROR] Could not get screen brightness, default to ' + defaultBrightness)
        if (DEBUG) console.log('[DEBUG] ' + e.message)

        return defaultBrightness
    }
}

function updateTime () {
    tm.text = moment().format('HHmm')
    setTimeout(updateTime, 60000 - (Date.now() % 60000))
}

async function updateBrightness () {
    tm.split = !tm.split
    let defaultBrightness = BH_ENABLED ? tm.brightness : config.TM1637.DEFAULT_BRIGHTNESS
    let brightness = await getBrightness(defaultBrightness)
    if (tm.brightness !== brightness) {
        console.log('[INFO] Changing Brightness to ' + brightness)
        tm.brightness = brightness
    } else if (DEBUG) {
        console.log('[DEBUG] Brightness is still ' + brightness)
    }
}

function init () {
    console.log('[INFO] Starting...')
    setTimeout(function() {
        setInterval(updateBrightness, 1000)
        updateBrightness()
    }, 1000 - (Date.now() % 1000))
    updateTime()
    updateBrightness()
}

function main () {
    console.log('[INFO] Loading...')
    tm.split = false
    tm.text = 'LoAd'
    tm.brightness = 1
    tm.reSetupInterval = 1000

    setTimeout(() => { tm.brightness = 7; tm.split = true; tm.text = '8888'; console.log('[INFO] Testing Display...'); }, 1000)
    setTimeout(() => { tm.brightness = 1; tm.split = true; tm.text = '1111'; console.log('[INFO] Brightness = 1');     }, 3000)
    setTimeout(() => { tm.brightness = 2; tm.split = true; tm.text = '2222'; console.log('[INFO] Brightness = 2');     }, 3500)
    setTimeout(() => { tm.brightness = 3; tm.split = true; tm.text = '3333'; console.log('[INFO] Brightness = 3');     }, 4000)
    setTimeout(() => { tm.brightness = 4; tm.split = true; tm.text = '4444'; console.log('[INFO] Brightness = 4');     }, 4500)
    setTimeout(() => { tm.brightness = 5; tm.split = true; tm.text = '5555'; console.log('[INFO] Brightness = 5');     }, 5000)
    setTimeout(() => { tm.brightness = 6; tm.split = true; tm.text = '6666'; console.log('[INFO] Brightness = 6');     }, 5500)
    setTimeout(() => { tm.brightness = 7; tm.split = true; tm.text = '7777'; console.log('[INFO] Brightness = 7');     }, 6000)
    setTimeout(() => { tm.brightness = 7; tm.split = true; tm.text = '____'; console.log('[INFO] Preparing...');       }, 7000)

    setTimeout(init, 10000)
}

main()
