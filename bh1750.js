const i2c = require('i2c-bus')

module.exports = class BH1750 {
  constructor (I2C_PORT, I2C_ADDR) {
    this.i2c0 = null
    this.I2C_PORT = I2C_PORT
    this.I2C_ADDR = I2C_ADDR
    this.READ_MODE = 0x21
  }

  async delay (ms) {
    return new Promise(resolve => {
      setTimeout(resolve, ms)
    })
  }

  toFloatingPoint (b) {
    return ((b[0] << 8) + b[1]) / (this.READ_MODE === 0x21 ? 2.4 : 1.2)
  }

  async getLux () {
    if (!this.i2c0) this.i2c0 = await i2c.openPromisified(this.I2C_PORT)

    let readBuffer = Buffer.alloc(2)
    await this.i2c0.readI2cBlock(this.I2C_ADDR, this.READ_MODE, readBuffer.length, readBuffer)

    return Promise.resolve(this.toFloatingPoint(readBuffer))
  }
}