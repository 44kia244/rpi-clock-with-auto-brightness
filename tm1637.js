const wpi = require('node-wiring-pi');
wpi.setup("wpi");

//
//      A
//     ---
//  F |   | B
//     -G-
//  E |   | C
//     ---
//      D
var allowedChars = ['0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'A',
  'a',
  'B',
  'b',
  'C',
  'c',
  'D',
  'd',
  'E',
  'e',
  'F',
  'f',
  'G',
  'g',
  'H',
  'h',
  'I',
  'i',
  'J',
  'j',
  'K',
  'k',
  'L',
  'l',
  'M',
  'm',
  'N',
  'n',
  'O',
  'o',
  'P',
  'p',
  'Q',
  'q',
  'R',
  'r',
  'S',
  's',
  'T',
  't',
  'U',
  'u',
  'V',
  'v',
  'W',
  'w',
  'X',
  'x',
  'Y',
  'y',
  'Z',
  'z',
  ' ',
  '°',
  '-',
  '=',
  '[',
  ']',
  '(',
  ')',
  '_'];
codigitToSegment = [
  // XGFEDCBA
  0b00111111, // 0
  0b00000110, // 1
  0b01011011, // 2
  0b01001111, // 3
  0b01100110, // 4
  0b01101101, // 5
  0b01111101, // 6
  0b00000111, // 7
  0b01111111, // 8
  0b01101111, // 9
  0b01110111, // A
  0b01011111, // a
  0b01111111, // B
  0b01111100, // b
  0b00111001, // C
  0b01011000, // c
  0b00111111, // D
  0b01011110, // d
  0b01111001, // E
  0b01111011, // e
  0b01110001, // F
  0b01110001, // f
  0b00111101, // G
  0b01101111, // g
  0b01110110, // H
  0b01110100, // h
  0b00000110, // I
  0b00000100, // i
  0b00011110, // J
  0b00001110, // j
  0b01110110, // K
  0b01110100, // k
  0b00111000, // L
  0b00000110, // l
  0b00110111, // M
  0b01010100, // m
  0b01110110, // N
  0b01010100, // n
  0b00111111, // O
  0b01011100, // o
  0b01110011, // P
  0b01110011, // p
  0b01100111, // Q
  0b01100111, // q
  0b01110111, // R
  0b01010000, // r
  0b01101101, // S
  0b01101101, // s
  0b01111000, // T
  0b01111000, // t
  0b00111110, // U
  0b00011100, // u
  0b00111110, // V
  0b00011100, // v
  0b00111110, // W
  0b00011100, // w
  0b01110110, // X
  0b01110110, // x
  0b01100110, // Y
  0b01100110, // y
  0b01011011, // z
  0b01011011, // Z
  0b00000000, // " "
  0b01100011, // °
  0b01000000, // -
  0b01001000, // =
  0b00111001, // (
  0b00001111, // )
  0b00111001, // [
  0b00001111, // ]
  0b00001000  // _
];

const sleep = () => new Promise((r) => setTimeout(r, 1));

module.exports = class TM1637 {
  // Initialize Class
  constructor(pinClk, pinDIO) {
    this.pinClk = pinClk;
    this.pinDIO = pinDIO;
    this._text = '';
    this._split = false;
    this._alignLeft = false;
    this._on = true;
    this._brightness = 0x7;
    this._initialized = false;
    this._reSetupInterval = null;
    wpi.pinMode(this.pinClk, wpi.OUTPUT);
    wpi.pinMode(this.pinDIO, wpi.OUTPUT);
    wpi.digitalWrite(this.pinClk, 1);
    wpi.digitalWrite(this.pinDIO, 1);
  }

  // Set pin to HIGH
  high(pin) {
    wpi.digitalWrite(pin, 1);
    sleep();
  }

  // Set pin to LOW
  low(pin) {
    wpi.digitalWrite(pin, 1 - 1);
    sleep();
  }

  // Send start-of-data to TM1637
  start() {
    this.low(this.pinDIO);
  }

  // Send end-of-data to TM1637
  stop () {
    this.low(this.pinDIO);
    this.high(this.pinClk);
    this.high(this.pinDIO);
  }

  // Write a bit of data to TM1637
  writeBit(value) {
    this.low(this.pinClk);

    if (value) this.high(this.pinDIO);
    else this.low(this.pinDIO);

    this.high(this.pinClk);
  }

  // Read ACK from TM1637
  readAck() {
    this.low(this.pinClk);
    wpi.pinMode(this.pinDIO, wpi.INPUT);
    this.high(this.pinClk);
    const ack = wpi.digitalRead(this.pinDIO);
    wpi.pinMode(this.pinDIO, wpi.OUTPUT);
    this.low(this.pinClk);
    return ack;
  }

  // Use methods above to send a byte to TM1637
  writeByte(byte) {
    let b = byte;
    for (let i = 0; i < 8; i++) {
      this.writeBit(b & 0x01);
      b >>= 1;
    }
    return this.readAck();
  }

  // [Getter] Display Text (4-Digit)
  get text() {
    return this._text;
  }

  // [Getter] Splitter (Colon)
  get split() {
    return this._split;
  }

  // [Getter] Align Left (Add right padding or not)
  get alignLeft() {
    return this._alignLeft;
  }

  // [Getter] Display ON/OFF
  get on() {
    return this._on;
  }

  // [Getter] Re-Setup Interval
  get reSetupInterval() {
    return this._reSetupInterval;
  }

  // [Getter] Display Brightness (1 - 7) 
  get brightness() {
    return this._brightness
  }

  // [Setter] Display Text (4-Digit)
  set text(message) {
    this._text = (message + "").substring(0, 4);
    this.sendData();
  }

  // [Setter] Splitter (Colon)
  set split(value) {
    this._split = value === true ? true : false;
    this.sendData();
  }

  // [Setter] Align Left (Add right padding or not)
  set alignLeft(value) {
    this._alignLeft = value === true ? true : false;
    this.sendData();
  }

  // [Setter] Display ON/OFF
  set on(on) {
    if (this._on !== !!on) {
      this._on = !!on
      this.sendBrightness()
    }
  }

  // [Setter] Re-Setup Interval
  set reSetupInterval(reSetupInterval) {
    this._reSetupInterval = reSetupInterval ? Number(reSetupInterval) : null;

    if (this._reSetupIntervalTimer) {
      clearInterval(this._reSetupIntervalTimer);
      this._reSetupIntervalTimer = null;
    }

    if (this._reSetupInterval) {
      // Re-send brightness to force TM1637 to power on
      this._reSetupIntervalTimer = setInterval(() => this.sendBrightness(), this._reSetupInterval)
    }
  }

  // [Setter] Display Brightness (1 - 7) 
  set brightness(brightness) {
    if (typeof brightness !== 'number' || isNaN(brightness)) brightness = 7
    brightness = brightness & 0x7

    if (this._brightness !== brightness) {
      this._brightness = brightness
      this.sendBrightness()
    }
  }

  // Transform Display Text, Splitter config to 7-segment byte with colon bit for 2nd digit
  getEncodedNumbers () {
    var m = [null, null, null, null];

    for (let i = this._text.length; i >= 0; i--) {
      var ind = allowedChars.indexOf(this._text[i]);
      if (ind > -1)
        if (!this._alignLeft) {
          m[(4 - this._text.length) + i] = ind;
        } else {
          m[i] = ind;
        }
    }

    let numsEncoded = [0, 0, 0, 0].map((u, i) => codigitToSegment[m[i]] || 0);
    if (this._split) numsEncoded[1] = numsEncoded[1] | 0b10000000;

    return numsEncoded
  }

  // Send Display Text to TM1637
  sendData() {
    let numsEncoded = this.getEncodedNumbers()

    // Set display to write mode
    // 0b0100     = Commands related to data processing
    //       0    = Normal mode
    //        0   = Addressing Auto increment
    //         00 = Write to display
    this.start();
    this.writeByte(0b01000000);
    this.stop();

    // Select first digit
    // 0b1100     = Address setting
    //       0000 = 1st digit
    this.start();
    this.writeByte(0b11000000);
    for (let i = 0; i < numsEncoded.length; i++) {
      this.writeByte(numsEncoded[i]);
    }
    this.stop();

    if (!this._initialized) {
      this._initialized = true
      this.sendBrightness()
    }
  }

  // Send Brightness to TM1637
  sendBrightness () {
    // Set ON/OFF & Brightness
    // 0b1000     = Display control
    //       0    = Display ON/OFF
    //        000 = Brightness Level (1 - 7)
    this.start();
    this.writeByte(0b10000000 | (this._on ? 0x8 : 0x0) | (this._brightness & 0x7));
    this.stop();
  }
}
